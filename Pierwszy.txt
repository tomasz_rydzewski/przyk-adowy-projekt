﻿PARYŻ

 

Patrz! przy zachodzie, jak z Sekwany łona

Powstają gmachy połamanym składem,

Jak jedne drugim wchodzą na ramiona,
// dodatek do
Gdzieniegdzie ulic przeświecone śladem.

Gmachy skręconym wydają się gadem,
fasg
Zębatą dachów łuską się najeża.

A tam - czy żądło oślinione jadem?

Czy słońca promień? czy spisa rycerza?
asdg
Wysoko - strzela blaskiem ozłocona wieża.

 

Nowa Sodomo! pośród twych kamieni
asdg
Mnoży się zbrodnia bezwstydna widomie

I kiedyś na cię spadnie deszcz płomieni,

Lecz nie deszcz boży, nie zamknięty w gromie,

Sto dział go poszle... A na każdym domie

Kula wyryje straszny wyrok Boga;

Kula te mury przepali, przełomie,

I wielka na cię spadnie kiedyś trwoga,

I większa jeszcze rozpacz - bo to kula wroga...

 

I już nad miastem wisi ta dział chmara,

Dlatego ludu zasępione tłumy,

Dlatego ciemność ulic tak ponura,

Przeczuciem nieszczęść zbłąkane rozumy;

Bez echa kona słowo próżnéj dumy,

O wrogach ciągłe toczą się rozmowy...

A straż ich przednią już północne dżumy

Obrońców ludu pozwiewały głowy,

I po ulicach ciągły brzmi dzwon pogrzebowy.

 

Czy wrócą, czasy tych świętych tajemnic,

Kiedy tu ludzie zbytkiem życia wściekli,

Jedni pod katem, drudzy w głębi ciemnic,

Inni ponurzy, bladzi, krwią ociekli,

Co kiedy śmieli pomyśleć - wyrzekli?

Lud cały kona, katy i obrońce,

Dnia im nie stało, aby się wysiekli;

I przeczuwając krwawéj zorzy końce,

Jak Jozue wołali: Dnia trzeba - stój, słońce?

 

I nie stanęło - pomarli - przedwcześnie,

Lecz zostawili pamiątki po sobie:

Kraj po rozlewie krwi tonący we śnie

I lud, nie po nich ubrany w żałobie,

Krwi trójcę w jednéj wcieloną osobie. *

Ten jak rodyjski posąg świecznik trzyma

I jedną nogę wsparł na martwych grobie,

Drugą na zamku królów... Gdzie oczyma

Sięgnął - tam wnet i ręką dostawał olbrzyma.

 

A kiedy posąg walił się z podstawy,

Tysiące ludu sławą się dzieliło,

Każdy się okrył łachmanem téj sławy,

Każdemu było dosyć - nadto było...

Marzą o dawnéj sławie nad mogiłą

I pod kolumną spiżu wszyscy posną; **

Choć cięcie kata głowę z niéj strąciło,

Choć na niéj może jak na gruzach z wiosną

Chwasty i z lilijami Burbonów porosną.

 

Tu dzisiaj Polak błąka się wygnany,

W nędzy - i brat już nie pomaga bratu.

Wierzby płaczące na brzegach Sekwany

Smutne są dla nas jak wierzby Eufratu.

I całéj nędzy nie wyjawię światu...

Twarze z marmuru - serca marmurowe,

Drzewo nadziei bez liścia i kwiatu

Schnie, gdy wygnaniec złożył pod nim głowę,

Jak nad prorokiem Judy schło drzewo figowe.

 

. . . . . . . . . . . . . . . . .

Z dala od miasta szukajmy napisów,

Gdzie wielki cmentarz zalega na górze. ***

O! jak tu smutno, kędy wśród cyprysów

Pobladłe w cieniu chowają się róże.

A pod stopami - daléj - miasto w chmurze

Topi się we mgłach gasnących opalu...

A dla żałobnych rodzin przy tym murze

Przędą ją wianki z płótna lub z perkalu,

Aby dłużéj świadczyły o kupionym żalu.

 

Patrz znów w mgłę miejską - oto wież ostatki,

Gotyckim kunsztem ukształcona ściana; ****

Rzekłbyś - że zmarła matka twojéj matki,

W czarne brabanckie korónki ubrana,

Z chmur się wychyla jak duch Ossyjana...

Ludzi nie dojrzysz... Lecz nad mgłami fali

Stoją posągi (gdzie płynie Sekwana), *****

Jakby się w Styksu łodzi zatrzymali

I przed piekła bramami we mgłach stoją biali...

* Napoleon...

** Kolumna Vendôme.

*** Cmentarz Père la Chaise

**** Koścół katedralny Notre-Dame.

***** Most Zgody - albo Ludwika XVI, z białemi posągami.

****** Po wzięciu Luwra na królewskim tronie lud położył trupa...